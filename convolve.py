import matplotlib.pyplot as plt
import numpy as np
import scipy as sp


class NormalDenFn:
    
    def __init__(self, domain, sd):
        self.domain = domain
        self.mean = 0.0
        self.sd = sd
        self.image = self.comp_image()
        self.third_moment = self.comp_third_moment()
        return None
    
    def comp_image(self):
        x = -(self.domain - self.mean)**2.0
        x /= 2.0 * self.sd**2.0
        image = np.exp(x)
        image /= self.sd*(2.*np.pi)**0.5
        # # Test 
        # w = (self.domain[-1] - self.domain[0])/self.domain.size
        # print(w*np.sum(image))
        return image
    
    def comp_third_moment(self):
        w = (self.domain[-1] - self.domain[0])/self.domain.size
        p = np.sum(w*np.abs((self.domain - self.mean)**3.0)*self.image)
        return p


class UniformDenFn:
    
    def __init__(self, domain, width):
        self.domain = domain
        self.mean = 0.0
        self.image = self.comp_image(width)
        self.sd = self.comp_sd(width)
        self.third_moment = self.comp_third_moment(width)
        return None
    
    def comp_image(self, width):
        height = 1.0/width
        mask = np.logical_and((self.domain > -width/2.0),
                              (self.domain < width/2.0))
        image = mask*height*np.ones(self.domain.shape)
        return image
    
    def comp_sd(self, width):
        height = 1.0/width
        sd = (width/2.0)**3.0
        sd /= 2.0
        sd *= 2.0*height
        sd = sd**0.5
        return sd
    
    def comp_third_moment(self, width):
        height = 1.0/width
        p = (width/2.0)**4.0
        p /= 3.0
        p *= 2.0*height
        return p
        
        
class ShiftedGammaDenFn:
    
    def __init__(self, domain, k):
        self.domain = domain
        self.mean = 0.0
        self.k = k
        self.theta = 1.0/k # To make the mean 0
        self.image = self.comp_image()
        self.sd = self.comp_sd()
        self.third_moment = self.comp_third_moment()
        return None
    
    def comp_image(self): 
        # Shift domain
        domain = self.domain + self.k 
        #
        image = np.exp(-domain/self.theta)
        image *= domain**(self.k - 1.0)
        image /= self.theta**self.k
        image /= sp.special.gamma(self.k)
        # Add zeros
        image *= (domain > 0)
        # # Test 
        # w = (self.domain[-1] - self.domain[0])/self.domain.size
        # print(w*np.sum(image))
        return image

    def comp_sd(self):
        sd = np.sqrt(self.k*self.theta**2.0)
        # w = (self.domain[-1] - self.domain[0])/self.domain.size
        # sd = np.sqrt(sum(w*(self.image - self.mean)**2.0))
        return sd
    
    def comp_third_moment(self):
        p = 2.0/np.sqrt(self.k)
        # w = (self.domain[-1] - self.domain[0])/self.domain.size
        # p = sum(w*np.abs(self.image - self.mean)**3.0)
        return p
    

def compute_mean_densities(den_fn, N_max):
    # Convolve
    w = (den_fn.domain[-1] - den_fn.domain[0])/den_fn.domain.size
    convolutions = []
    convolution = den_fn.image.copy()
    for _ in range(N_max-1):
        convolution = w*sp.signal.fftconvolve(convolution, den_fn.image, 
                                              mode='same')
        convolution /= np.sum(w*convolution) # For rounding errors
        convolutions.append(convolution.copy())
    # Rescale and pad
    mean_den_images = [den_fn.image]
    for i in range(N_max-1): 
        N = i+2
        convolution = convolutions[i]
        # Rescale
        # Starting at N//2 is to get the midpoints
        mean_den_image = N*convolution[::N]
        # mean_den_image /= np.sum(w*mean_den_image) # For rounding errors
        # Pad
        pad_len_1 = (den_fn.domain.size - mean_den_image.size)//2
        pad_len_2 = (den_fn.domain.size - mean_den_image.size) - pad_len_1
        mean_den_image = np.pad(mean_den_image, (pad_len_1, pad_len_2), 
                                'constant', constant_values=(0.0, 0.0))
        #
        mean_den_images.append(mean_den_image)
    #
    return mean_den_images


def plot_approximations(den_fn):
    domain = den_fn.domain
    # 
    fig, axs = plt.subplots(1, 4, figsize=(12, 4))
    Ns = [1, 2, 5, 30]
    mean_den_images = compute_mean_densities(den_fn, Ns[-1])
    for i, N in enumerate(Ns): 
        mean_den_image = mean_den_images[i]
        normal = NormalDenFn(domain, den_fn.sd/np.sqrt(np.sqrt(N)))
        normal_image = normal.image
        axs[i].plot(domain, mean_den_image, label='exact', linestyle='--')
        axs[i].plot(domain, normal_image, label='normal', linestyle=':')
        #
        axs[i].set_title(f'N = {N}')
        axs[i].set_xlabel(f'mean')
        axs[i].set_xlim(-1.75, 1.75)
        axs[i].set_ylim(0.0, 1.5)
    #
    axs[0].set_ylabel('probability density')
    axs[0].legend(loc='upper left')
    plt.savefig('convolution.png')
    plt.show() 
    #
    return None


def plot_cum_den_fns(den_fn):
    domain = den_fn.domain
    w = (domain[-1] - domain[0])/domain.size
    # 
    fig, axs = plt.subplots(1, 4, figsize=(12, 4))
    Ns = [1, 2, 5, 30]
    mean_den_images = compute_mean_densities(den_fn, Ns[-1])
    for i, N in enumerate(Ns): 
        mean_den_image = mean_den_images[i]
        normal = NormalDenFn(domain, den_fn.sd/np.sqrt(np.sqrt(N)))
        normal_image = normal.image
        # 
        emp_cum = w*np.cumsum(mean_den_image)
        normal_cum = w*np.cumsum(normal_image)
        diff = np.abs(emp_cum - normal_cum)
        #
        axs[i].plot(domain, emp_cum, label='empirical')
        axs[i].plot(domain, normal_cum, label='normal')
        axs[i].plot(domain, diff, label='difference')
        axs[i].set_title(f'N = {N}')
        axs[i].set_xlabel(f'mean')
        axs[i].set_xlim(-1.75, 1.75)
        axs[i].set_ylim(-0.05, 1.05)
    #
    axs[0].set_ylabel('')
    axs[0].legend(loc='upper left')
    plt.savefig('cumulative.png')
    plt.show() 
    #
    return None


def plot_convergence(den_fn):
    domain = den_fn.domain
    w = (domain[-1] - domain[0])/domain.size
    # 
    # Ns = list(range(1, 50))
    Ns = [1, 2, 5, 20, 30, 40, 50]
    mean_den_images = compute_mean_densities(den_fn, Ns[-1])
    errors = []
    for i, N in enumerate(Ns): 
        mean_den_image = mean_den_images[i]
        normal = NormalDenFn(domain, den_fn.sd/np.sqrt(np.sqrt(N)))
        normal_image = normal.image
        # 
        emp_cum = w*np.cumsum(mean_den_image)
        normal_cum = w*np.cumsum(normal_image)
        diff = np.abs(emp_cum - normal_cum)
        error = np.max(diff)
        errors.append(error)
        #
    # Get theoretical bounds        
    C = 0.4748 
    Bs = []
    p = den_fn.third_moment
    sd = den_fn.sd
    for N in Ns:
        B = C*p/(sd**3)
        B /= np.sqrt(N)
        Bs.append(B)
    # Plot 
    plt.figure(figsize=(12, 4))
    plt.plot(Ns, errors, label='empirical error')
    plt.plot(Ns, Bs, label='theoretical bound')
    plt.xlabel("N")
    plt.legend()
    plt.savefig('bound.png')
    plt.show()
    #
    return None


def main():
    domain = np.linspace(-5.0, 5.0, 100_000)
    # den_fn = NormalDenFn(domain, 1.0)
    den_fn = UniformDenFn(domain, 2.0)
    # den_fn = ShiftedGammaDenFn(domain, 1.0)
    plot_approximations(den_fn)
    plot_cum_den_fns(den_fn)
    plot_convergence(den_fn)
    #
    return None    


if __name__ == '__main__':
    main()