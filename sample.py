import math as ma
import matplotlib.pyplot as plt
import numpy as np
import statistics as stats


def get_samples():
    num_samples = 100
    samples = np.random.poisson(1, num_samples)
    return samples


def get_mean_samples(num_trials):
    means = []
    for _ in range(num_trials):
        samples = get_samples()
        mean = samples.mean()
        means.append(mean)
    return means


def normal_density(min, max, mean, sd):
    x = np.linspace(min, max, 1000)
    y = -(1./2.) * ((x - mean)/sd)**2.
    y = np.exp(y)
    y /= sd*ma.sqrt(2.*ma.pi)
    return x, y 


def make_subplot(ax, num_trials):
    # Empirical density 
    means = get_mean_samples(num_trials)
    ax.hist(means, label=f'empirical',
             bins=40, histtype='step', density=True)
    # Theoretical density
    mean = stats.mean(means) # sample mean of all samplings
    sd = stats.stdev(means) # sample standard deviation of the means
    domain, image = normal_density(min(means), max(means), mean, sd)  
    ax.plot(domain, image, label='theoretical')
    #
    ax.set_xlabel('mean')
    ax.set_title(f'{num_trials} trials')
    #
    return None


def main():
    fig, axs = plt.subplots(1, 4, figsize=(12, 4))
    for i in range(4):
        num_trials = 10**(i+1)
        make_subplot(axs[i], num_trials)    
    #
    axs[0].set_ylabel('probability density')
    axs[0].legend(loc='upper left')
    # plt.savefig('sample.png')
    plt.show() 
    #
    return None


if __name__ == '__main__':
    main()
