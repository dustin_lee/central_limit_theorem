# Empirical Verification of the Central Limit Theorem

## Running 

Run either `sample.py` or `convolve.py` to empirically verify the Central Limit Theorem. These programs are explain in the section [Sampling the Mean](#samplings-of-the-mean) and [Convolution of the Probability Distribution](#convolution-of-the-probability-density-function), respectively. 


## The Central Limit Theorem 

The [Central Limit Theorem](https://en.wikipedia.org/wiki/Central_limit_theorem)
says that, for any sequence $(X_i)_{i≥1}$ of identically distributed random variables with mean $μ$ and standard deviation $σ$, and for real number $ε$, there exists a positive integer $N$ such that the relation 
$$
    \mathrm{dist}_{\mathrm{D}} \left( 
        \frac{X_1 + \cdots + X_N}{N},
        \mathcal{N}\left(μ, \frac{σ^2}{\sqrt{N}}\right)
    \right)
    < ε
$$
holds, where $\mathcal{N}$ is the [normal density function](https://en.wikipedia.org/wiki/Normal_distribution#Standard_normal_distribution) and the distance is [in distribution](https://en.wikipedia.org/wiki/Convergence_of_random_variables#Convergence_in_distribution). 
Furthermore, the [Berry-Essen Theorem](https://en.wikipedia.org/wiki/Berry%E2%80%93Esseen_theorem) says that the relation
$$
    \mathrm{dist}_{\mathrm{D}} \left( 
        \frac{X_1 + \cdots + X_N}{N},
        \mathcal{N}\left(μ, \frac{σ^2}{\sqrt{N}}\right)
    \right)
    < \frac{C⋅p}{N⋅σ^2}
$$
holds, where $C$ is a constant that is at most $0.4748$ and $ρ$ is the third moment of $X_1$. 


## Samplings of the Mean

The program `sample.py` verifies the Central Limit Theorem by sampling the mean of the random variable and comparing this to the normal distribution.

In the code, the normal density functions use sample means and sample standard deviations instead of the population mean and population standard deviation. 
Furthermore, it uses the sample mean of means rather than the sample mean of the total population. 

| ![](sample.png) |
| :---: |
| *Empirical verification of the central limit theorem using sampling with the Poisson distribution.* | 


## Convolution of the Probability Density Function  

Let $X_1$, $\dots$, $X_{N-1}$ and $X_N$ be random variables. 
And, let $f_1$ $\dots$, $f_{N-1}$ and $f_N$ be their probability density functions, respectively. 
Then, the probability density function of their sum 
$$
   X_1 + \cdots + X_N
$$
is the convolution
$$
    (f_1*\cdots*f_N)(x).
$$
of their probability densities. 
By a change of variables, the probability density function for the random variable 
$$
    \frac{X_1 + \cdots + X_N}{N}
$$
is 
$$
    N⋅(f_1*\cdots*f_N)(x/N). 
$$
Thus, the distribution 
$$
    \frac{X_1 + \cdots + X_N}{N}
$$
in the Central Limit Theorem can be computed using convolution. 


| ![](convolution.png) |
| :---: |
| *Empirical verification of the central limit theorem using convolution with the uniform distribution* | 

| ![](cumulative.png) |
| :---: |
| *Empirical verification of the Berry-Essen Theorem for the uniform distribution by comparing of the cumulative density functions* | 

| ![](bound.png) |
| :---: |
| *Empirical verification of the the Berry-Essen Theorem for the uniform distribution by comparison the errors* | 